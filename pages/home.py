from playwright.sync_api import expect


class HomePage:
    def __init__(self, page):
        self.page = page

    def check_email_in_screen(self, email):
        expect(self.welcome_message_in_screen).to_have_text(email)


    @property
    def welcome_message_in_screen(self):
        return self.page.get_by_text(f"Hello there")