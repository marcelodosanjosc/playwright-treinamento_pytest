from pages.auth_saleor import AuthPage
from playwright.sync_api import Page


class TestAuth:

    def test_login_valid(self, page: Page):
        auth = AuthPage(page)
        auth.navigate()
        auth.login("admin@example.com", "admin")
        auth.check_login_success()

    def test_login_invalid(self, page: Page):
        auth = AuthPage(page)
        auth.navigate()
        auth.login("admiasd", "admi")
        auth.check_login_fail()