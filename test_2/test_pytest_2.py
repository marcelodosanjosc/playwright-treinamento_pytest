import pytest

# Configuração de sessão
@pytest.fixture(scope="session", autouse=True)
def setup_session():
    print("Configuração de sessão")

    # Opcionalmente, você pode retornar dados ou objetos úteis para os testes
    yield

    print("Desmontando sessão")

# Configuração de módulo
@pytest.fixture(scope="module", autouse=True)
def setup_module():
    print("Configuração de módulo")

    # Opcionalmente, você pode retornar dados ou objetos úteis para os testes
    yield

    print("Desmontando módulo")


# Configuração de classe
@pytest.fixture(scope="class", autouse=True)
def setup_class():
    print("Configuração de classe")

    # Opcionalmente, você pode retornar dados ou objetos úteis para os testes
    yield

    print("Desmontando classe")


# Configuração de função
@pytest.fixture(scope="function", autouse=True)
def setup_function():
    print("Configuração de função")

    # Opcionalmente, você pode retornar dados ou objetos úteis para os testes
    yield

    print("Desmontando função")


# Exemplo de teste
class TestExemplo:
    def test1(self):
        print("Executando o teste 1")

    def test2(self):
        print("Executando o teste 2")


# Execute os testes
pytest.main()
