import pytest


class Robo:

    def __init__(self, nome, bateria=100):
        self.nome = nome
        self.bateria = bateria

    def apresentar(self):
        if self.bateria > 0:
            self._consumir_bateria(5)
            return f"Olá, eu sou o robô {self.nome}."
        else:
            return "Desculpe, estou sem energia para me apresentar."

    def mover(self, direcao):
        if self.bateria > 10:
            self._consumir_bateria(10)
            return f"Robô {self.nome} se moveu para {direcao}."
        else:
            return "Desculpe, estou sem energia para me mover."

    def recarregar(self):
        self.bateria = 100
        return f"Robô {self.nome} recarregado."

    def _consumir_bateria(self, quantidade):
        self.bateria -= quantidade


@pytest.fixture
def robo_com_bateria_cheia():
    return Robo("Robozinho", bateria=100)


@pytest.fixture
def robo_com_pouca_bateria():
    return Robo("Robozinho", bateria=5)


class TestRobo:

    def test_apresentar_com_bateria_cheia(self, robo_com_bateria_cheia):
        resultado = robo_com_bateria_cheia.apresentar()
        assert resultado == "Olá, eu sou o robô Robozinho."
        assert robo_com_bateria_cheia.bateria == 95

    def test_apresentar_com_pouca_bateria(self, robo_com_pouca_bateria):
        resultado = robo_com_pouca_bateria.apresentar()
        assert resultado == "Olá, eu sou o robô Robozinho."
        assert robo_com_pouca_bateria.bateria == 0

    def test_mover_com_bateria_cheia(self, robo_com_bateria_cheia):
        resultado = robo_com_bateria_cheia.mover("frente")
        assert resultado == "Robô Robozinho se moveu para frente."
        assert robo_com_bateria_cheia.bateria == 90

    def test_mover_com_pouca_bateria(self, robo_com_pouca_bateria):
        resultado = robo_com_pouca_bateria.mover("frente")
        assert resultado == "Desculpe, estou sem energia para me mover."
        assert robo_com_pouca_bateria.bateria == 5

    def test_recarregar(self, robo_com_pouca_bateria):
        resultado = robo_com_pouca_bateria.recarregar()
        assert resultado == "Robô Robozinho recarregado."
        assert robo_com_pouca_bateria.bateria == 100
