from playwright.sync_api import sync_playwright, expect
import re
import pytest


@pytest.fixture(scope="session")
def browser_context_args(browser_context_args, playwright):
    playwright.selectors.set_test_id_attribute("data-test-id")
    playwright.chromium.launch(headless=False)
    return {
        **browser_context_args,
        "storage_state": "storage.json",
        "viewport": {
            "width": 1920,
            "height": 1080,
        }
    }


def test_run():
    playwright = sync_playwright().start()
    playwright.selectors.set_test_id_attribute('data-test-id')
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    page = context.new_page()
    page.goto("https://demo.saleor.io/dashboard/")
    page.locator("input[name=email]").fill("admin@example.com")
    page.locator("input[name=password]").fill("admin")
    page.get_by_role("button", name="Sign in").click()
    expect(page.get_by_text("Hello there, admin@example.com"), "Não localizado email do admin").to_be_visible()
    expect(page.locator("data-test-id=menu-list-item")).to_have_text(["Products", "Orders", "Discounts"])
    expect(page.locator("data-test-id=menu-list").locator("a")).to_contain_text(["Home", "Products", "Orders",
                                                                                 "Customers", "Discounts", "Content",
                                                                                 "Translations", "Apps"])


def test_saleor_auth():
    playwright = sync_playwright().start()
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    page = context.new_page()
    page.goto("https://demo.saleor.io/dashboard/")
    playwright.selectors.set_test_id_attribute("data-test-id")
    email = page.get_by_test_id('email')
    email.fill('admin@example.com')
    password = page.get_by_test_id("password")
    password.fill("admin")
    signin = page.get_by_role("button", name="SigN In")
    signin.click()
    expect(page.get_by_text("Hello there, admin@example.com"), "Não localizado email do admin").to_be_visible()
    expect(page.locator("data-test-id=menu-list-item")).to_have_text(["PRODUCTS", "Orders", "Discounts"],
                                                                     ignore_case=True)
    expect(page.get_by_test_id("menu-list").locator("a")).to_contain_text(["Home", "Products", "Orders",
                                                                           "Customers", "Discounts", "Content",
                                                                           "Translations", "Apps", "Configuration"])
    expect(page.locator("data-test-id=menu-list").locator("a")).to_have_count(9)


def test_seleor_activity():
    playwright = sync_playwright().start()
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    page = context.new_page()
    page.goto("https://demo.saleor.io/dashboard/")
    playwright.selectors.set_test_id_attribute("data-test-id")
    email = page.get_by_test_id('email')
    email.fill('admin@example.com')
    password = page.get_by_test_id("password")
    password.fill("admin")
    signin = page.get_by_role("button", name="SigN In")
    signin.click()
    page.get_by_text("Hello there, admin@example.com").wait_for(timeout=3000)
    page.locator("ul.MuiList-roo > li")
    expect(page.locator("ul.MuiList-roo > li").all()).to_have_count(10)
    activity_list = page.locator("ul.MuiList-roo > li").all()

    for item in activity_list:
        item.inner_text()


def test_saleor_vanilton():
    playwright = sync_playwright().start()
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    page = context.new_page()
    page.goto("https://demo.saleor.io/dashboard/")
    playwright.selectors.set_test_id_attribute("data-test-id")
    email = page.get_by_test_id('email')
    email.fill('admin@example.com')
    password = page.get_by_test_id("password")
    password.fill("admin")
    signin = page.get_by_role("button", name="SigN In")
    signin.click()
    context.storage_state(path="storage.json")
    page.get_by_text("Hello there, admin@example.com").wait_for(timeout=5000)
    expect(page.locator("ul.MuiList-root > li")).to_have_count(10)
    activity_list = page.locator("ul.MuiList-root > li").all()
    # 1. Criar um localizador para identificar a lista de atividades
    orders = []
    # 2. Deve ser possível iterar nos itens da lista 3. Criar uma lista de Ordens extraída da lista de atividades,
    # considerar apenas o números das ordens, pode-se utilizar regex
    for item in activity_list:
        orders.append(re.search(r'#(\d+)', item.inner_text()).group(1))
    # pages.locator("xpath=//ul[contains(@class, 'MuiList-root')]/li")

    # 4. Abrir uma página nova e na tela de pesquisa de ordens, consultar os números das ordens contidas na lista
    page2 = context.new_page()
    page2.goto("https://demo.saleor.io/dashboard/orders?asc=false&sort=number")

    total = 0
    for order in orders:
        page2.get_by_test_id("search-input").fill(order)
        expect(page2.locator('//td[@aria-colindex="2"]')).to_have_text(order)
        total += float(page2.locator("id=glide-cell-6-0").text_content())

    assert total == 1714.20


def test_pytest_page():
    playwright = sync_playwright().start()
    browser = playwright.chromium.launch(headless=False)
    context = browser.new_context()
    page = context.new_page()
    page.goto("https://demo.saleor.io/dashboard/")
    playwright.selectors.set_test_id_attribute("data-test-id")
